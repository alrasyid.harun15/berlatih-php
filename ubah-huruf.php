<?php
function ubah_huruf($string){
	
	$str_len = strlen($string);
	$oput = "";
	for($s=0;$s<$str_len;$s++){
		
		$oput .= chr((ord($string[$s])+1));
		
	}
	return $oput . "<br>" ;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>