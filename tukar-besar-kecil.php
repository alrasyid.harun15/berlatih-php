<?php
function tukar_besar_kecil($string){

	$str_len = strlen($string);
	$oput = "";
	for($s=0;$s<$str_len;$s++){
			if( ctype_upper($string[$s])){
				$oput .= strtolower($string[$s]);
			}elseif( ctype_lower($string[$s])){
				$oput .= strtoupper($string[$s]);
			}else{
				$oput .= $string[$s];
			}
		
	}
	return $oput . "<br>";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>